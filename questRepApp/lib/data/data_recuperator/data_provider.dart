import 'package:application2/data/models/question.dart';
import 'package:application2/data/repositories/questions_repositories.dart';
import 'package:flutter/cupertino.dart';
class DataRecuperator{

  final QuestionsRepository repository = new QuestionsRepository();

  static List<Question>getAllQuestions(){
    QuestionsRepository.getAllQuestions();
    return QuestionsRepository.questions;
  }
  static void addQuestion(String text, String isCorrect, String thematic,BuildContext context ){
    bool isC = false;
    isCorrect = isCorrect.toLowerCase();
    if(isCorrect== "vrai"){
      isC = true;
    }
    Question question = Question(question: text, isCorrect: isC, thematic: thematic);
    QuestionsRepository.addQuestions(question, context);
  }
}