part of 'questions_quiz_cubit.dart';

@immutable
abstract class QuestionsQuizState {}

class QuestionsQuizInitial extends QuestionsQuizState {
  List<Question> questions ;
  int index;
  int score;
  ThemeMode themeMode;
  QuestionsQuizInitial(this.questions, this.index, this.score, this.themeMode);

}
