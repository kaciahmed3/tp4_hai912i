
import 'package:application2/data/data_recuperator/data_provider.dart';
import 'package:application2/data/models/question.dart';
import 'package:application2/views/screens/result.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';

part 'questions_quiz_state.dart';

class QuestionsQuizCubit extends Cubit<QuestionsQuizState> {

  List<Question> _questions=[] ;
  int _index=0 ;
  int _score=0;
  ThemeMode _themeMode;

  QuestionsQuizCubit(this._questions,this._index,this._score,this._themeMode) : super(QuestionsQuizInitial(_questions,0,0,ThemeMode.system));

  void nextQuestion(BuildContext context) {
    if (index < questions.length - 1) {
      index++;
    } else {
      Navigator.pushReplacement(
          context,
          MaterialPageRoute(
              builder: (context) => Result(
                  score: _score
              )));
    }
    emit(QuestionsQuizInitial(questions, index, score,_themeMode));
  }
  void checkAnswer(bool userChoice, BuildContext context){
    if (questions[index].isCorrect==userChoice) {
      _score++ ;
      nextQuestion(context);
    } else {
      _score--;
      nextQuestion(context);
    }
    emit(QuestionsQuizInitial(questions, index, score,_themeMode));
  }
  void restart(){
    index =0;
    score=0;
    _questions = DataRecuperator.getAllQuestions();
    emit(QuestionsQuizInitial(questions, index, score,_themeMode));
  }
  bool get isDarkMode => _themeMode == ThemeMode.dark;

  void toggleTheme(bool isOn){
 /*  if(isOn){
     if(ThemeMode.system.index== ThemeMode.light.index){
       _themeMode =ThemeMode.dark;
     }else{
       _themeMode =ThemeMode.light;
     }
    }else{
     _themeMode=ThemeMode.system;
   }
*/

  _themeMode =isOn ? ThemeMode.dark : ThemeMode.system;
    emit(QuestionsQuizInitial(questions, index, score,_themeMode));
  }


  int get score => _score;

  set score(int value) {
    _score = value;
  }

  int get index => _index;

  set index(int value) {
    _index = value;
  }

  List<Question> get questions => _questions;

  set questions(List<Question> value) {
    _questions = value;
  }

  ThemeMode get themeMode => _themeMode;

  set themeMode(ThemeMode value) {
    _themeMode = value;
  }
}
