
import 'package:application2/data/data_recuperator/data_provider.dart';
import 'package:application2/data/theme_data/my_theme.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:application2/views/screens/homepage.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:application2/business_logic/cubits/question_quiz_cubit/questions_quiz_cubit.dart';


void main() async{
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());

}
class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
        create: (_) => QuestionsQuizCubit(DataRecuperator.getAllQuestions(),0,0,ThemeMode.system),
        child: BlocBuilder<QuestionsQuizCubit, QuestionsQuizState >(
            builder: (context, theme) {
              final themeP =context.watch<QuestionsQuizCubit>();
              return MaterialApp(
                themeMode:  themeP.themeMode,
                theme: MyThemes.lightTheme,
                darkTheme: MyThemes.darkTheme,
                home: HomePage(),
              );
            }
        )
    );

  }
}


