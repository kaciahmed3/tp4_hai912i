import 'package:application2/business_logic/cubits/question_quiz_cubit/questions_quiz_cubit.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ChangeThemeButtonWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final themeProvider = context.read<QuestionsQuizCubit>();

    return Switch.adaptive(
      value: themeProvider.isDarkMode,
      onChanged: (value) {
        final provider = context.read<QuestionsQuizCubit>();
        provider.toggleTheme(value);
      },
    );
  }
}