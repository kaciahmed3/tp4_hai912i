import 'package:application2/business_logic/cubits/question_quiz_cubit/questions_quiz_cubit.dart';
import 'package:application2/data/data_recuperator/data_provider.dart';
import 'package:application2/views/widgets/change_theme_button_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:application2/views/screens/homepage.dart';
import 'package:provider/src/provider.dart';



class AddQuestionPage extends StatefulWidget{
  @override
  _AddQuestionPageState createState() => _AddQuestionPageState();
}

class _AddQuestionPageState extends State<AddQuestionPage> {
  TextEditingController valText = TextEditingController();
  TextEditingController valIsCorrect =  TextEditingController();
  TextEditingController valThematic =  TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: const Center(
            child: Text("Ajouter question"),
          ),
        backgroundColor: Colors.redAccent,
        actions: [
          ChangeThemeButtonWidget(),
        ],
      ),
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 30,vertical: 50),
        width: MediaQuery.of(context).size.width,
        child: SingleChildScrollView(
             child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                          TextFormField(
                           controller: valText,
                          autofocus: false,
                          decoration: const InputDecoration(
                            filled: true,
                            hintText: 'Text de la question',
                          ),
                        ),
                        SizedBox(height: 30),
                    TextFormField(
                      controller: valIsCorrect,
                      autofocus: false,
                      decoration: InputDecoration(
                        filled: true,
                        hintText: 'vrai/ faux',
                      ),
                    ),
                    SizedBox(height: 30),
                    TextFormField(
                      controller: valThematic,
                      autofocus: false,
                      decoration: InputDecoration(
                        filled: true,
                        hintText: 'thématique',
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    GestureDetector(
                      onTap: () {
                        DataRecuperator.addQuestion(valText.text, valIsCorrect.text, valThematic.text, context);
                        context.read<QuestionsQuizCubit>().restart();
                        Navigator.pushReplacement(context,
                            MaterialPageRoute(builder: (context) => HomePage()));
                      },
                      child: Container(
                        padding: EdgeInsets.symmetric(vertical: 12, horizontal: 54),
                        child: Text(
                          "Ajouter",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 18,
                              fontWeight: FontWeight.w500),
                        ),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(24),
                            color: Colors.blue),
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    GestureDetector(
                      onTap: () {
                        context.read<QuestionsQuizCubit>().restart();
                        Navigator.pushReplacement(context,
                            MaterialPageRoute(builder: (context) => HomePage()));
                      },
                      child: Container(
                        padding: EdgeInsets.symmetric(vertical: 12, horizontal: 54),
                        child: Text(
                          "Retour",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 18,
                              fontWeight: FontWeight.w500),
                        ),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(24),
                            color: Colors.blue),
                      ),
                    ),
                  ],
              ),
        ),
      ),
    );
  }

}