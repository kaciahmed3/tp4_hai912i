import 'package:application2/business_logic/cubits/question_quiz_cubit/questions_quiz_cubit.dart';import 'package:application2/views/screens/homepage.dart';
import 'package:application2/views/widgets/change_theme_button_widget.dart';
import 'package:flutter/material.dart';
import 'package:provider/src/provider.dart';

class Result extends StatefulWidget {
  int score;
  Result({required this.score});

  @override
  _ResultState createState() => _ResultState();
}

class _ResultState extends State<Result> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Center(
          child: Text("Résultats"),
        ),
        backgroundColor: Colors.redAccent,
        actions: [
          ChangeThemeButtonWidget(),
        ],
      ),
      body: Container(
        width: MediaQuery.of(context).size.width,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            SizedBox(
              height: 14,
            ),
            Text("Score : ${widget.score} ",
              style: TextStyle(
                fontSize: 28,
                fontWeight: FontWeight.w500,
              ),
            ),
            SizedBox(
              height: 16,
            ),
            GestureDetector(
              onTap: () {
                context.read<QuestionsQuizCubit>().restart();
                Navigator.pushReplacement(context,
                    MaterialPageRoute(builder: (context) => HomePage()));
              },
              child: Container(
                padding: EdgeInsets.symmetric(vertical: 12, horizontal: 54),
                child: Text(
                  "Accueil",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 18,
                      fontWeight: FontWeight.w500),
                ),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(24),
                    color: Colors.blue),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
