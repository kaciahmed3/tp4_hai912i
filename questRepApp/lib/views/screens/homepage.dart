import 'package:application2/views/screens/add_question_page.dart';
import 'package:application2/views/widgets/change_theme_button_widget.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:application2/views/screens/play_quiz.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Center(
         child: Text("Questions /Réponses"),
        ),
        backgroundColor: Colors.redAccent,
        actions: [
          ChangeThemeButtonWidget(),
        ],
      ),
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 30),
        width: MediaQuery.of(context).size.width,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            GestureDetector(
              onTap: (){
                Navigator.pushReplacement(context, MaterialPageRoute(
                    builder: (context) => PlayQuiz()
                ));
              },
              child: Container(
                padding: EdgeInsets.symmetric(vertical: 12,horizontal: 54),
                decoration: BoxDecoration(
                    color: Colors.blue,
                    borderRadius: BorderRadius.circular(24)
                ),
                child: Text("Lancer le quiz ", style: TextStyle(
                    color: Colors.white,
                    fontSize: 18
                ),),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            GestureDetector(
              onTap: (){
                Navigator.pushReplacement(context, MaterialPageRoute(
                    builder: (context) => AddQuestionPage()
                ));
              },
              child: Container(
                padding: EdgeInsets.symmetric(vertical: 12,horizontal: 54),
                decoration: BoxDecoration(
                    color: Colors.blue,
                    borderRadius: BorderRadius.circular(24)
                ),
                child: Text("Ajouter une Question ", style: TextStyle(
                    color: Colors.white,
                    fontSize: 18
                ),),
              ),
            ),

          ],
        ),
      ),
    );
  }
}